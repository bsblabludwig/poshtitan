#!/usr/bin/bash
################################################################################
#
# Process Kraken output into a report. Curate the report for analysis. Grab desired number of top pathogen hits. Extract reads assigned to the top hit number.
# Run fastqc, stampy, and bowtie on extracted reads. Analyze the mapping output for read origen, and coverage. 
# 
#
# Author: David Severson
#
################################################################################

##USE: $0 <out.directory> <pathogen.rank.threshold> <raw.kraken.output.gz> <fastq_1.gz> <fastq_2.gz>



#DEPENDENCY: kraken-report <raw.raken.output.gz> <out.directory>
#DEPENDENCY: krakenoutput_processor.sh  <kraken_report_file> 
#DEPENDENCY: krakenreport_to_read_id.sh  <krakenoutput_processor.sh file.output> <pathogen.rank.threshold> <raw.kraken.output.gz> <fastq_1.gz> <fastq_2.gz> <out.directory>
#DEPENDENCY: titan_bowtie2.all.sh
#DEPENDENCY: titan_stampy.all.sh
#DEPENDENCY: titan_bowtie2.ind.sh
#DEPENDENCY: titan_stampy.ind.sh

### Inform the user of option setup and check for viability by number.
if (( $# == 5 )); then 
	echo "Assuming you have input the options <out.directory> <pathogen.rank.threshold> <raw.kraken.output.gz> <fastq_1.gz> <fastq_2.gz>;"
	echo "Then, the number of options suggest everything is in order for paired-end reads titan analysis."
elif (( $# == 4 )); then
	echo "Assuming you have input the options <out.directory> <pathogen.rank.threshold> <raw.kraken.output.gz> <fastq.gz>;" 
	echo "Then, the number of options suggest everything is in order for single-end reads titan analysis."
else 
	echo "Something is wrong with the number of options:"
	echo "Please use <out.directory> <pathogen.rank.threshold> <raw.kraken.output.gz> <fastq_1.gz> <fastq_2.gz> for paired-end reads titan analysis."
	echo "Or <out.directory> <pathogen.rank.threshold> <raw.kraken.output.gz> <fastq.gz>  for single-end reads titan analysis."
	exit;
fi
###

### Sort output directory and run kraken-report on output
if [ -d $1 ]; then
	echo "The output directory $1 already exists; we will use this directory. Are you sure analysis has not been previously performed?"
else
	mkdir $1
fi

if [[ $3 =~ ".gz" ]]; then
	echo "Unzipping the file on the fly for the Kraken script, kraken-report, to analyze."
	gunzip -c $3 | kraken-report --db /mnt/lustre/data/kraken/kraken_db  - > ${1}/${3}.report
else
	echo "Your file is already unzipped; please run again on gzipped file" && exit;  
fi
##

### Process Kraken reports
echo "Processing Kraken-report output for analysis using krakenoutput_processor.sh."
sh /mnt/lustre/data/dseverson/poshTitan/titan/krakenoutput_processor.sh ${3}.report $1
###


### Obtain fastq files for each species
echo "Grabbing sub-fastq files for each species using krakenreport_to_read_id.sh"
sh /mnt/lustre/data/dseverson/poshTitan/titan/krakenreport_to_read_id.sh ${3}.report.root_normalized.species $2 $3 $4 $5 $1

### Organize output folder by putting reports, fastqfiles, and clade info into three distinct directories
cdir=`pwd`
cd $1
[ ! -d fastq_files ] &&  mkdir fastq_files
fastq=`ls | grep -ci -e "\.fastq"`
(( $fastq > 0 )) && mv *.fastq fastq_files/
[ ! -d kraken_reports ] && mkdir kraken_reports
reports=`ls | grep -ci -e "\.report"`
(( $reports  > 0 )) && mv *.report* kraken_reports/ 
[ ! -d clades_info ] && mkdir clades_info
clades=`ls | grep -ci -e "\.clades"`
(( $clades > 0 )) && mv *.clades* clades_info/
###

### Perform mapping on species organized fastq files using Stampy and Bowtie 2 on individual relevant reference genomes
echo "Now, performing species-specific mapping with Bowtie2!"
bash /mnt/lustre/data/dseverson/poshTitan/titan/titan_bowtie2.ind.sh fastq_files


###Sequential mapping if low memory machine - takes much longer... 
###bash /mnt/lustre/data/dseverson/poshTitan/titan/titan_bowtie2.seq.sh fastq_files



##move files to the appropriate directory
mkdir species-specific_mapping
remaps=`ls fastq_files | grep -ci -e "specific_remapping_bt2"`
(( $remaps > 0 )) && mv fastq_files/*specific_remapping_bt2* species-specific_mapping 
###

## Unbiased Coverage Regardless of Mapping Amount
head -n 20 species-specific_mapping/*specific_remapping_bt2/*.coverage | grep gi | sort -nr -k7,7| head -n 50 | cut -d"|" -f 2 > species-specific_mapping/most_coverage.temp
head -n 20 species-specific_mapping/*specific_remapping_bt2/*.coverage | grep gi | sort -nr -k7,7| head -n 50 > species-specific_mapping/top_coverage.temp
sort -t"|" -k2,2 species-specific_mapping/top_coverage.temp > top_coverage.sorted.temp
grep -F -f species-specific_mapping/most_coverage.temp /mnt/lustre/data/dseverson/poshTitan/whole_genome/pathogens.sizes | sort -t"|" -k2,2 | cut -f 1 > species-specific_mapping/names.temp
cut -d"|" -f 5 top_coverage.sorted.temp | paste species-specific_mapping/names.temp - | sort -nr -k7,7 > top_hits_coverage.txt
[ -f species-specific_mapping/most_coverage.temp ] && rm species-specific_mapping/most_coverage.temp;
[ -f top_coverage.sorted.temp ] && rm top_coverage.sorted.temp;
[ -f species-specific_mapping/top_coverage.temp ] && rm species-specific_mapping/top_coverage.temp;
[ -f species-specific_mapping/names.temp ] && rm species-specific_mapping/names.temp
###

###Make coverage files more user friendly
cd species-specific_mapping/
for MAPPINGS in $(echo *_bt2);
do
	cd ${MAPPINGS}
	CHECK=$(echo *.coverage | sed -e 's/ .*//g')
	if [ $CHECK != '*.coverage' ]; then
		for COV in $(echo *.coverage);
		do 
			SPECIESN=`echo $COV | sed -e 's/\..*uid/|uid/g' -e 's/\..*//g'`
			sed -e "s/.*ref/${SPECIESN}/g" $COV | cut -f 1,4-7 > temp.${COV}
			mv temp.${COV} $COV
		done
	else 
		echo "There were no references for $MAPPINGS"		
	fi
	cd ..
done

echo "Collating the coverage and mapping results into a useable format for R.."

### Obtain flagstats and then determing coverage for highest mappers 
#Check to ensure we don't append to an already existing failing after an aborted run...
[ -f ../top_subspecies_coverage.contig.txt ] && rm ../top_subspecies_coverage.contig.txt
[ -f ../top_subspecies_mappings.pre.txt ] && rm ../top_subspecies_mappings.pre.txt
[ -f ../top_subspecies_coverage.pre.txt ] && rm ../top_subspecies_coverage.pre.txt


for SPECIES in $(echo *_bt2); 
do  
	ppreads_last=0 
	ppreads=0
	reads_last=0
	reads=-1
	for BAM in $(echo ${SPECIES}/*.bam); 
	do 
		[ -f $BAM ] && ppreads=`samtools flagstat $BAM | grep "properly paired" | cut -d " " -f 1`
		[ -f $BAM ] && reads=`samtools flagstat $BAM | grep "mapped (" | cut -d " " -f 1 `
		(( $ppreads > $ppreads_last )) && samtools flagstat $BAM > temp1.${SPECIES} && echo $BAM | sed -e 's|.*/||g' -e 's/.sort.bam/.coverage/g' | cat - temp1.${SPECIES} > temp.${SPECIES} && ppreads_last=$ppreads
		(( $ppreads == $ppreads_last )) && (( $reads >= $reads_last )) &&  samtools flagstat $BAM > temp1.${SPECIES} && echo $BAM | sed -e 's|.*/||g' -e 's/.sort.bam/.coverage/g' | cat - temp1.${SPECIES} > temp.${SPECIES} && ppreads_last=$ppreads && reads_last=$reads 
	done 
	SPECIES2=`echo $SPECIES | sed -e 's/\..*//g'`
	[ -f temp.${SPECIES} ] && echo ">${SPECIES2}" |  cat - temp.${SPECIES} >> ../top_subspecies_mappings.pre.txt &&  rm temp1.${SPECIES} && rm temp.${SPECIES}
	COVERAGE=`grep -e ".coverage" ../top_subspecies_mappings.pre.txt | grep -e "${SPECIES2}"`
	[ -f ${SPECIES}/${COVERAGE} ] && awk -v OFS="\t" '{ SUMR += $2; SUMC += $3; SUMT += $4;  print $1,SUMR,SUMC,SUMT,SUMC/SUMT*100}' ${SPECIES}/${COVERAGE} | tail -n 1 >> ../top_subspecies_coverage.pre.txt
	[ -f ${SPECIES}/${COVERAGE} ] && echo ">${SPECIES2}" | cat - ${SPECIES}/${COVERAGE} >> ../top_subspecies_coverage.contig.txt
done
###
cd ..
pwd
###Format Mapping Stats and Coverage Stats for R input
grep "^>" top_subspecies_mappings.pre.txt > names.temp; 
grep "properly paired" top_subspecies_mappings.pre.txt | cut -d " " -f 1   > pp.temp ; 
grep "with itself" top_subspecies_mappings.pre.txt | cut -d " " -f 1 > discordant.temp; 
grep "singleton" top_subspecies_mappings.pre.txt | cut -d " " -f 1 > singleton.temp; 
grep "paired in" top_subspecies_mappings.pre.txt | cut -d " " -f 1 > total.temp;
echo "Species" | cat - names.temp > n.temp
echo "Concordant" | cat - pp.temp > p.temp
echo "Discordant" | cat - discordant.temp > d.temp
echo "Singletons" | cat - singleton.temp > s.temp
echo "Unmapped" | cat - total.temp > t.temp 
paste n.temp t.temp s.temp d.temp p.temp | sed -e 's/>//g' > top_subspecies_mappings.txt
head -n 1 top_subspecies_mappings.txt > cnames.temp
echo "Percent.Concordant" | paste cnames.temp - > cnames2.temp
tail -n +2 top_subspecies_mappings.txt | awk -v OFS="\t" '{ mapped=$3+$4; unmapped = $2-mapped; print $1,unmapped,$3,$4-$5,$5,$5/$2*100}' | sort -gr -k6,6 > data.temp
cat cnames2.temp data.temp > top_subspecies_mappings.txt
sort -gr -k5,5 top_subspecies_coverage.pre.txt  > top_subspecies_coverage.txt.temp
echo "Species"$'\t'"Mapped.reads"$'\t'"Covered"$'\t'"Genome.size"$'\t'"Percent.covered" | cat - top_subspecies_coverage.txt.temp > top_subspecies_coverage.txt
rm *.temp
###

rm top_subspecies_coverage.pre.txt
rm top_subspecies_mappings.pre.txt


### Return to top level directory
cd $cdir
pwd
###

echo "PoshTitan has performed the Olympian analysis in true dapper form."


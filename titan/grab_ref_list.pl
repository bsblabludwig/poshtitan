#!/usr/bin/perl
################################################################################
#
# Grab the references that correspond to GI's associated with all subclade ids
# 
#
# Author: David Severson
# GItoSubclade file path will have to be renamed when transferring pipeline
################################################################################

use warnings;
use strict;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use IO::File;

# Use $0 -q <subclade list> < <stdin of tax to ids> ; 
my $q = "";
my $j = 1;
GetOptions ("q=s" => \$q) or die("Error in command line arguments");
my %subclades;
my $k= 1;
my %gi_number;


print STDERR "Reading subclade  strings...\n";
my $query = IO::File->new($q, "r") or die "Something is wrong with the subclades file given\n";
while (<$query>) {
        chomp $_ ;
	my @columns = split(/\t/);
        $subclades{$columns[2]} = $j++;
#	print STDERR "$columns[2]\n";
}
$query->close();
print STDERR "Done reading ".scalar(keys %subclades)." clades, now creating hash of corresponding gi IDS...\n";
while (<STDIN>) {
	chomp();
	my @columns = split(/\t/, $_);
        if ($subclades{$columns[1]}) {
		print "$_ \n"; 
	}
}
print STDERR "Perl script has finished picking out references associated with the given species\n";

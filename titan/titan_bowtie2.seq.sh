#!/usr/bin/bash
################################################################################
#
# Run Bowtie2 on all clade sub-fastq files identified by kraken
#
#
# Author: David Severson
# Requires: Installation of bowtie2, samtools, and bedtools in PATH  
################################################################################

#USE $0 < directory of fastq files>
current=`pwd`;
cd $1;
files=$(ls *.clades*.fastq | sed -e 's/clades.*/clades/g' | sort -u);
for i in ${files[@]};
do 
 
	k=0;
	mkdir ${i}_specific_remapping_bt2
	dir=${i}_specific_remapping_bt2
	echo $current
	pwd
	wc -l ../clades_info/${i}.subclades
	perl /mnt/lustre/data/dseverson/poshTitan/titan/grab_ref_list.pl -q ../clades_info/${i}.subclades < /mnt/lustre/data/dseverson/poshTitan/titan/file2taxon.titan.map > ${dir}/${i}_reference_genome.list
	ref=$(cut -f 1 ${dir}/${i}_reference_genome.list);
	refs=`wc -l ${dir}/${i}_reference_genome.list | cut -d" " -f 1`;
	echo "Aligning ${i} using bowtie2 to all relevant reference genomes: ${ref[@]}"; 	
	for j in ${ref[@]}
	do
		name=$(echo $j | sed -e 's/.*uid/uid/g')
		echo "${k}: Aligning ${i} to reference, ${j}, and sending it to background until all $refs references have been aligned."
		bowtie2 -k 1 --very-sensitive -N 1 --local -X 1000 -t -p 8 -1 ${i}_1.fastq -2 ${i}_2.fastq -x /mnt/lustre/data/dseverson/poshTitan/ind_ref/bowtie2/${j} | samtools view -h -bS -> ${dir}/${i}.${name}.bt2.bam
		(( ++k == $refs )) && echo "Waiting on all relevent references to be aligned" && wait;
	done
	echo "Species files, ${i}, have been aligned to all relevant references"
	k=0;
	for j in ${ref[@]}
	do
		name=$(echo $j | sed -e 's/.*uid/uid/g')
        	samtools sort ${dir}/${i}.${name}.bt2.bam ${dir}/${i}.${name}.bt2.sort
 		(( ++k == $refs )) && echo "Waiting on all relevant bam files to be sorted" && wait;	
	done
	for j in ${ref[@]}
        do
 		name=$(echo $j | sed -e 's/.*uid/uid/g')
                rm ${dir}/${i}.${name}.bt2.bam
		genomeCoverageBed -dz -ibam ${dir}/${i}.${name}.bt2.sort.bam -g /mnt/lustre/data/dseverson/poshTitan/ind_ref/sizes/${j}.sizes >  ${dir}/${i}.${name}.bt2.bdg 
	       	bedtools coverage -abam ${dir}/${i}.${name}.bt2.sort.bam -b /mnt/lustre/data/dseverson/poshTitan/ind_ref/sizes/${j}.sizes.bed | sort -rn -k4,4 > ${dir}/${i}.${name}.bt2.coverage
                read_number=$(samtools flagstat ${dir}/${i}.${name}.bt2.sort.bam | grep total| cut -d+ -f 1)
                echo -e "${i}.${name}.bt2.sort.bam\t${read_number}" >> ${dir}/read_numbers_temp.txt
                sort -u ${dir}/read_numbers_temp.txt > ${dir}/read_numbers.txt  
	done
	rm ${dir}/read_numbers_temp.txt
done

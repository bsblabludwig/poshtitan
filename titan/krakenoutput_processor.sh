#!/usr/bin/bash
################################################################################
#
# Process Kraken Reports for use by second mapper. 
# 
#
# Author: David Severson
#
################################################################################

#Use $0 <kraken_report_file> <target directory>

##Fix report for R use
#cp ${2}/${1} ${2}/${1}.archive
#cut -f 1-5 ${2}/${1} > report.temp1
#cut -f 6 ${2}/${1} | sed -e 's/^[ ]*//g' > report.temp2
#paste report.temp1 report.temp2 > ${2}/${1} 	
#rm report.temp1
#rm report.temp2

echo "Creating root normalized file"
root=$(grep -w ${2}/${1} -e "root" | cut -f 2)
echo $root
awk -v OFS="\t"  -v k=$root '{print $0, $2/k*100}' ${2}/$1 | sort -r -g -k3,3 | cut -f 7 > temp.root
awk -v OFS="\t"  -v k=$root '{print $0, $2/k*100}' ${2}/$1 | sort -r -g -k3,3 | cut -f 2-6 | paste temp.root - > ${2}/${1}.root_normalized 
rm temp.root

echo "Performing Species Level Analysis" 
awk -v OFS=\t '($4 == "S") { print $0 }' ${2}/${1}.root_normalized | sort -rn -k2,2 | cut -f 1-5 -> temp.txt
awk -v OFS=\t '($4 == "S") { print $0 }' ${2}/${1}.root_normalized | sort -rn -k2,2 | cut -f 6 | sed 's/^[ \t]*//' > temp2.txt
paste temp.txt temp2.txt > ${2}/${1}.root_normalized.species
rm temp.txt
rm temp2.txt

echo "Performing Genus Level Analysis"
awk -v OFS=\t '($4 == "G") { print $0 }' ${2}/${1}.root_normalized | sort -rn -k2,2 | cut -f 1-5 -> temp.txt
awk -v OFS=\t '($4 == "G") { print $0 }' ${2}/${1}.root_normalized | sort -rn -k2,2 | cut -f 6 | sed 's/^[ \t]*//' > temp2.txt
paste temp.txt temp2.txt > ${2}/${1}.root_normalized.genus
rm temp.txt
rm temp2.txt

echo "Performing Family Level Analysis"
awk -v OFS=\t '($4 == "F") { print $0 }' ${2}/${1}.root_normalized | sort -rn -k2,2 | cut -f 1-5 -> temp.txt
awk -v OFS=\t '($4 == "F") { print $0 }' ${2}/${1}.root_normalized | sort -rn -k2,2 | cut -f 6 | sed 's/^[ \t]*//' > temp2.txt
paste temp.txt temp2.txt > ${2}/${1}.root_normalized.family
rm temp.txt
rm temp2.txt

echo "Performing Order Level Analysis"
awk -v OFS=\t '($4 == "O") { print $0 }' ${2}/${1}.root_normalized | sort -rn -k2,2 | cut -f 1-5 -> temp.txt
awk -v OFS=\t '($4 == "O") { print $0 }' ${2}/${1}.root_normalized | sort -rn -k2,2 | cut -f 6 | sed 's/^[ \t]*//' > temp2.txt
paste temp.txt temp2.txt > ${2}/${1}.root_normalized.order
rm temp.txt
rm temp2.txt

echo "Creating Order, Family, Genus, Species Combined File"
cat  ${2}/${1}.root_normalized.species ${2}/${1}.root_normalized.genus ${2}/${1}.root_normalized.family ${2}/${1}.root_normalized.order | sort -rn -k2,2 > ${2}/${1}.root_normalized.ofgs.rooted
cat  ${2}/${1}.root_normalized.species ${2}/${1}.root_normalized.genus ${2}/${1}.root_normalized.family ${2}/${1}.root_normalized.order | sort -rn -k3,3 > ${2}/${1}.root_normalized.ofgs.unrooted  

#!/usr/bin/bash
################################################################################
#
# Run Bowtie2 on all clade sub-fastq files identified by kraken
#
#
# Author: David Severson
# Requires: Installation of bowtie2, samtools, and bedtools in PATH  
################################################################################

#USE $0 < directory of fastq files>
current=`pwd`;
cd $1;
files=$(ls *.clades*.fastq | sed -e 's/clades.*/clades/g' | sort -u);
ref=$(ls /home/ubuntu/fastqfiles/pathogen_references/whole_genome/bowtie2/*.bt2 | sed -e 's/\..*//g' | sort -u); 
echo "${files[@]}";
for i in ${files[@]};
do 
	k=1;
	mkdir ${i}_remapping_bt2
	dir=${i}_remapping_bt2
	echo "Aligning ${i} using bowtie2."; 	
	for j in ${ref[@]}
	do
		name=$(echo $j | sed -e 's/.*bowtie2\///g')
		echo "${name}" 
		echo "${k}: Aligning ${i} to reference, ${j}, and sending it to background until all three references have been aligned."
		( bowtie2 -k 1 -N 1 --local -X 1000 -t -p 4 -1 ${i}_1.fastq -2 ${i}_2.fastq -x ${j} | samtools view -bS -> ${dir}/${i}.${name}.bt2.bam ) &
		(( ++k == 4 )) && wait;
	done
	echo "${i} has been aligned to all three references"
	k=1;
	for j in ${ref[@]}
	do
		name=$(echo $j | sed -e 's/.*bowtie2\///g')
        	( samtools sort ${dir}/${i}.${name}.bt2.bam ${dir}/${i}.${name}.bt2.sort ) &
 		(( ++k == 4 )) && wait;	
	done
	for j in ${ref[@]}
        do
 		name=$(echo $j | sed -e 's/.*bowtie2\///g')
                rm ${dir}/${i}.${name}.bt2.bam
		genomeCoverageBed -dz -ibam ${dir}/${i}.${name}.bt2.sort.bam -g  /home/ubuntu/fastqfiles/pathogen_references/whole_genome/bowtie2/${name}.bt2.sizes >  ${dir}/${i}.${name}.bt2.bdg 
        	bedtools coverage -abam ${dir}/${i}.${name}.bt2.sort.bam -b /home/ubuntu/fastqfiles/pathogen_references/whole_genome/bowtie2/${name}.bt2.sizes.bed | sort -rn -k4,4 > ${dir}/${i}.${name}.bt2.coverage
                read_number=$(samtools flagstat ${dir}/${i}.${name}.bt2.sort.bam | grep total| cut -d+ -f 1)
                echo -e "${i}.${name}.bt2.sort.bam\t${read_number}" >> ${dir}/read_numbers_temp.txt
                sort -u ${dir}/read_numbers_temp.txt > ${dir}/read_numbers.txt  
	done
	rm ${dir}/read_numbers_temp.txt
done

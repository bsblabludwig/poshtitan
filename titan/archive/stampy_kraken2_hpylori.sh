#!/usr/bin/bash
################################################################################
#
# Run bwa-assisted STAMPY on all clade sub-fastq files identified by kraken 
# Reads are mapped to the four reference genomes that together make up the entire pathogen reference database
#
# Author: David Severson
#
################################################################################


##run the script in local directory contained sub-fastq files identified by kraken -> ./stampy_kraken.sh
##no options
files=$(ls *.clades*.fastq | sed -e 's/clades.*/clades/g' | sort -u);
#ref=$(ls ~/pathogen_references/stampy/* | sed -e 's/\..*//g' | sort -u)
ref=$(ls /home/ubuntu/pathogen_references/stampy/Helicobacter_only/ | sed -e 's/\..*//g' | sort -u)
bwaref=$(ls ~/pathogen_references/stampy/*.fa) 
for i in ${files[@]};
do 
	k=1;
	mkdir ${i}_stampy_map_analysis
	echo "Mapping ${i} now with 8 threaded bwa."
#	for j in ${bwaref[@]} 
#	do 
		pairs=(1 2)		
		m=0;
		for l in ${pairs[@]};
		do  
			echo "${m}: Generating ${k}.${l}.sai for ${i}."
			( bwa aln  -q10 -t8 /home/ubuntu/pathogen_references/stampy/Helicobacter_only/path2.ref.stampy.H.Pylori.fa ${i}_${l}.fastq > ${k}.${l}.sai ) & 
			(( ++m == 2 )) &&  wait 
		done
		echo "${k}: Starting bwa mapping of ${i} to the pathogen reference, ${j}, and sending it to background until all references have been mapped."
		bwa sampe -P -a 5000 -N 0 /home/ubuntu/pathogen_references/stampy/Helicobacter_only/path2.ref.stampy.H.Pylori.fa ${k}.1.sai ${k}.2.sai ${i}_1.fastq ${i}_2.fastq | samtools view -bS -> ${i}_stampy_map_analysis/${i}.path${k}.bwa.bam 
#		(( ++k == 2 )) &&  wait 
#	done
	indices=(1)
	for l in ${indices[@]}
	do
		rm ${l}.1.sai
		rm ${l}.2.sai
	done
	k=1
	echo "Using stampy to fine tune mapping of ${i}."
#	for j in ${ref[@]}
#	do
#		echo "${k}: Sending stampy mapping of ${i} to pathogen reference, ${j}, to the background until all four pieces have mapped"
		 ~/stampy/stampy.py --insertsize=1000 --insertsd=500 --substitutionrate=0.20 --sensitive -g /home/ubuntu/pathogen_references/stampy/Helicobacter_only/hp1 -h /home/ubuntu/pathogen_references/stampy/Helicobacter_only/hp1 -t6 --bamkeepgoodreads -M ${i}_stampy_map_analysis/${i}.path${k}.bwa.bam | samtools view -bS -> ${i}_stampy_map_analysis/${i}.path${k}.stampy.bam
#		(( ++k == 5 )) &&  wait 
#	done
#	k=1
#	for j in ${ref[@]}
#	do
		samtools sort ${i}_stampy_map_analysis/${i}.path${k}.stampy.bam ${i}_stampy_map_analysis/${i}.path${k}.stampy.sort 
#		(( ++k ==5 )) && wait 
#	done
	for k in ${indices[@]}
	do
		rm ${i}_stampy_map_analysis/${i}.path${k}.stampy.bam
		rm ${i}_stampy_map_analysis/${i}.path${k}.bwa.bam
		genomeCoverageBed -dz -ibam ${i}_stampy_map_analysis/${i}.path${k}.stampy.sort.bam -g  ~/pathogen_references/stampy/Helicobacter_only/H.Pylori.sizes >  ${i}_stampy_map_analysis/${i}.path${k}.stampy.bdg 
		bedtools coverage -abam ${i}_stampy_map_analysis/${i}.path${k}.stampy.sort.bam -b ~/pathogen_references/stampy/Helicobacter_only/H.Pylori.sizes.bed | sort -rn -k4,4 > ${i}_stampy_map_analysis/${i}.path${k}.stampy.coverage
		read_number=$(samtools flagstat ${i}_stampy_map_analysis/${i}.path${k}.stampy.sort.bam | grep total| cut -d+ -f 1)
		echo -e "${i}.path${k}.stampy.sort\t$read_number" >> ${i}_stampy_map_analysis/read_numbers_temp.txt
		sort -u ${i}_stampy_map_analysis/read_numbers_temp.txt > ${i}_stampy_map_analysis/read_numbers.txt  
	done
done	
		

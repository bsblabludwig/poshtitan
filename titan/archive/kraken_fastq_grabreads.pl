#!/usr/bin/perl
################################################################################
#
# Call Ben Schuster's Read Filter and Iteratively perform on all chromosomes
# Then recombine all files
#
# Author: David Severson
#
################################################################################

use warnings;
use strict;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use IO::File;

my $q = "";
my $f = "";
my $j = 1;
GetOptions ("f=s" => \$f, "q=s" => \$q) or die("Error in command line arguments");
my %queries;

print STDERR "Reading query strings...\n";
my $query = IO::File->new($q, "r") or die "Something is wrong with the processed kraken report you supplied. Please use syntax: krakenreport_to_read_id <output of krakenoutput_processor.sh> <threshold for number of organisms to re-map> <raw kraken output file> <fastq file>";
while (<$query>) {
	my @columns = split;	
	$queries{$columns[1]} = $j++;
}
print STDERR "There are $j clade numbers from which to identify reads.\n";
$query->close();

print STDERR "Done reading ".scalar(keys %queries)." clades, now filtering...\n";
$j=0;
my $buf="";
while (<STDIN>) {
	unless($j++ % 4) {
		my ($ident) = split(/\n/, $buf);
		if ($ident) {
			$ident =~ s/^\@(.*)\/\d.*/$1/;
			print $buf if (exists $queries{$ident});
		}
		$buf="";
	}
	$buf .= $_;
}

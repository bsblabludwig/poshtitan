#!/usr/bin/perl

################################################################################
#
# Grab the references that correspond to GI's associated with all subclade ids
# 
#
# Author: David Severson
# GItoSubclade file path will have to be renamed when transferring pipeline
################################################################################

use warnings;
use strict;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use IO::File;

# Use $0 -r <pathogen references> -q <subclade list> < <stdin of gi to clade file> ; 
my $q = "";
my $r = "";
my $j = 1;
GetOptions ("r=s" => \$r, "q=s" => \$q) or die("Error in command line arguments");
my %subclades;
my $k= 1;
my %gi_number;


print STDERR "Reading subclade  strings...\n";
my $query = IO::File->new($q, "r") or die "Something is wrong with the subclades file given\n";
while (<$query>) {
        chomp $_ ;
	my @columns = split(/\t/);
        $subclades{$columns[0]} = $j++;
#	print STDERR "$columns[0]\n";
}
$query->close();
print STDERR "Done reading ".scalar(keys %subclades)." clades, now creating hash of corresponding gi IDS...\n";
while (<STDIN>) {
	chomp();
	my @columns = split(/\t/, $_);
        if ($subclades{$columns[1]}) { 
		$gi_number{$columns[0]} = $columns[1] ;
	} 
}
print STDERR "The number of GI numbers is ".scalar(keys %gi_number)."\n"; 
print STDERR "Done creating gi ID hash, now filtering pathogen reference for relevant fasta files..\n";
my $reference = IO::File->new($r, "r") or die "Something is wrong with the reference fasta pathogen database supplied.\n";
while (<$reference>) {
	chomp();
	my @columns = split(/\|/, $_);	
	my $size = @columns;
	next if $size < 3;
	if ($gi_number{$columns[1]}) {
		print "$_\t$gi_number{$columns[1]}\n";
	}
}
print STDERR "Perl script has finished picking out references associated with the given species\n";

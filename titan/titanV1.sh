#!/usr/bin/bash
################################################################################
#
# Process Kraken output into a report. Curate the report for analysis. Grab desired number of top pathogen hits. Extract reads assigned to the top hit number.
# Run fastqc, stampy, and bowtie on extracted reads. Analyze the mapping output for read origen, and coverage. 
# 
#
# Author: David Severson
#
################################################################################

##USE: $0 <out.directory> <pathogen.rank.threshold> <raw.kraken.output.gz> <fastq_1.gz> <fastq_2.gz>



#DEPENDENCY: kraken-report <raw.raken.output.gz> <out.directory>
#DEPENDENCY: krakenoutput_processor.sh  <kraken_report_file> 
#DEPENDENCY: krakenreport_to_read_id.sh  <krakenoutput_processor.sh file.output> <pathogen.rank.threshold> <raw.kraken.output.gz> <fastq_1.gz> <fastq_2.gz> <out.directory>
#DEPENDENCY: titan_bowtie2.all.sh
#DEPENDENCY: titan_stampy.all.sh
#DEPENDENCY: titan_bowtie2.ind.sh
#DEPENDENCY: titan_stampy.ind.sh

### Inform the user of option setup and check for viability by number.
if (( $# == 5 )); then 
	echo "Assuming you have input the options <out.directory> <pathogen.rank.threshold> <raw.kraken.output.gz> <fastq_1.gz> <fastq_2.gz>;"
	echo "Then, the number of options suggest everything is in order for paired-end reads titan analysis."
elif (( $# == 4 )); then
	echo "Assuming you have input the options <out.directory> <pathogen.rank.threshold> <raw.kraken.output.gz> <fastq.gz>;" 
	echo "Then, the number of options suggest everything is in order for single-end reads titan analysis."
else 
	echo "Something is wrong with the number of options:"
	echo "Please use <out.directory> <pathogen.rank.threshold> <raw.kraken.output.gz> <fastq_1.gz> <fastq_2.gz> for paired-end reads titan analysis."
	echo "Or <out.directory> <pathogen.rank.threshold> <raw.kraken.output.gz> <fastq.gz>  for single-end reads titan analysis."
	exit;
fi
###

### Sort output directory and run kraken-report on output
if [ -d $1 ]; then
	echo "The output directory $1 already exists; we will use this directory. Are you sure analysis has not been previously performed?"
else
	mkdir $1
fi
#if [[ $3 =~ ".gz" ]]; then
#	echo "Unzipping the file on the fly for the Kraken script, kraken-report, to analyze."
#	gunzip -c $3 | kraken-report --db /mnt/lustre/data/kraken/kraken_stnd_db  - > ${1}/${3}.report
#else
#	echo "Your file is already unzipped; please run again on gzipped file" && exit;  
#fi
###

### Process Kraken reports
#echo "Processing Kraken-report output for analysis using krakenoutput_processor.sh."
#sh /mnt/lustre/data/dseverson/poshTitan/titan/krakenoutput_processor.sh ${3}.report $1
###

### Obtain fastq files for each species
echo "Grabbing sub-fastq files for each species using krakenreport_to_read_id.sh"
sh /mnt/lustre/data/dseverson/poshTitan/titan/krakenreport_to_read_id.sh ${3}.report.root_normalized.species $2 $3 $4 $5 $1

### Organize output folder by putting reports, fastqfiles, and clade info into three distinct directories
cdir=`pwd`
cd $1
[ ! -d fastq_files ] &&  mkdir fastq_files
fastq=`ls | grep -ci -e "\.fastq"`
#echo $fastq
(( $fastq > 0 )) && mv *.fastq fastq_files/
[ ! -d kraken_reports ] && mkdir kraken_reports
reports=`ls | grep -ci -e "\.reports"`
#echo $reports
(( $reports  > 0 )) && mv *.reports* kraken_reports/ 
[ ! -d clades_info ] && mkdir clades_info
clades=`ls | grep -ci -e "\.clades"`
#echo $clades
(( $clades > 0 )) && mv *.clades* clades_info/
###

### Perform mapping on species organized fastq files using Stampy and Bowtie2 on whole bacterial reference genome
#echo "Now, performing mapping using Stampy and Bowtie2!"
#bash /mnt/lustre/data/dseverson/poshTitan/titan/titan_bowtie2.all.sh fastq_files
#bash /mnt/lustre/data/dseverson/poshTitan/titan/titan_stampy.all.sh fastq_files
## Need script to collate information based on clade number and an R script to analyze Kraken read characteristics...
#[ ! -d bowtie2.all.maps] && mkdir bowtie2.all.maps
#bowties=`ls | grep -ci -e "remapping_bt2"`
#(( $bowties > 0 )) && mv *remapping_bt2 bowtie2.all.maps
#[ ! -d stampy.all.maps] && mkdir stampy.all.maps
#stamps=`ls | grep -ci -e "stampy_map_analysis"`
#(( $stamps > 0 )) && mv *stampy_map_analysis stampy.all.maps
###

### Perform mapping on species organized fastq files using Stampy and Bowtie 2 on individual relevant reference genomes
echo "Now, performing species-specific mapping with Bowtie2!"
bash /mnt/lustre/data/dseverson/poshTitan/titan/titan_bowtie2.ind.sh fastq_files
# bash ~/titan/titan_stampy.ind.sh
mkdir species-specific_mapping
remaps=`ls | grep -ci -e "specific_remapping_bt2"`
(( $remaps > 0 )) && mv *specific_remapping_bt2* species-specific_mapping 


## Need script to collate information based on clade number and an R script to analyze Kraken read characteristics...
cd fastq_files;
head -n 20 species-specific_mapping/*specific_remapping_bt2/*.coverage | grep gi | sort -nr -k7,7| head -n 50 | cut -d"|" -f 2 > species-specific_mapping/most_coverage.temp
head -n 20 species-specific_mapping/*specific_remapping_bt2/*.coverage | grep gi | sort -nr -k7,7| head -n 50 > species-specific_mapping/top_coverage.temp
sort -t"|" -k2,2 species-specific_mapping/top_coverage.temp > top_coverage.sorted.temp
grep -F -f species-specific_mapping/most_coverage.temp /mnt/lustre/data/dseverson/poshTitan/whole_genome/pathogens.sizes | sort -t"|" -k2,2 | cut -f 1 > species-specific_mapping/names.temp
cut -d"|" -f 5 top_coverage.sorted.temp | paste species-specific_mapping/names.temp - | sort -nr -k7,7 > top_hits.coverage
[ -f species-specific_mapping/most_coverage.temp ] && rm species-specific_mapping/most_coverage.temp;
[ -f top_coverage.sorted.temp ] && rm top_coverage.sorted.temp;
[ -f species-specific_mapping/top_coverage.temp ] && rm species-specific_mapping/top_coverage.temp;
[ -f species-specific_mapping/names.temp ] && rm species-specific_mapping/names.temp
cd .. 
###

### Perform fastq control analysis on all subsets of reads
#mkdir fastqc_analysis
#fastqc --nogroup -o fastqc_analysis  -t 8 fastq_files/*.fastq
###

#Obtain quality info about reads seperated by pathogen using fastqc and my own script to analyze kraken k-mer content




### Return to top level directory
cd $cdir
###

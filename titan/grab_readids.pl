#!/usr/bin/perl
################################################################################
#
# Build species read id list by grabbing all readids flagged by Kraken as 
# belonging to a subclade of that species
#
# Author: David Severson
#
################################################################################

use warnings;
use strict;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use IO::File;

my $q = "";
my $j = 1;
GetOptions ("q=s" => \$q) or die("Error in command line arguments");
my %queries;

print STDERR "Reading query strings...\n";
my $query = IO::File->new($q, "r") or die "Something is wrong with the subclades file given\n";
while (<$query>) {
        chomp $_;
	my @columns = split(/\t/);
        $queries{$columns[2]} = $j++;
#	print STDERR "$columns[2]\n";
}
$query->close();

print STDERR "Done reading ".scalar(keys %queries)." clades, now filtering...\n";
while (<STDIN>) {
	my (@columns) = split(/\t/);
#	print STDERR "$columns[2]\n";
        if ($queries{$columns[2]}) { 
		print;
	}
}
print STDERR "Perl script has finished analyzing Kraken output file for subclade assigned reads\n";

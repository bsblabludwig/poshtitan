##!/usr/bin/bash
################################################################################
#
# Use species tag to grab all taxonomy ids of subclades rooted in that species, then grab reads from original fastq file .
# When moving to the new cluster the name of the perl script will have to be modified to accomodate the new path to the titan folder!
#
#
# Author: David Severson
#Institute: Ludwig Institute for Cancer Research
################################################################################

##Please use output from running krakenoutput_processor.sh on Kraken report output...
##USE: $0 <output of krakenoutput_processor.sh> <threshold for number of organisms to re-map> <raw kraken output file> <gzip compressed fastq file> <gzip compress fastq file> <target directory> <gzip compressed fastq_file>
[ -a ${6}/${1}.runtime.report ] && rm ${6}/${1}.runtime.report


echo "The following pathogens will be analyzed by second mapping:" >> ${6}/${1}.runtime.report
echo "${6}/${1}"
ls ${6}/${1}
head -n $2 ${6}/${1} >> ${6}/${1}.runtime.report
clades=$(cut -f 5 ${6}/${1} | head -n $2)
echo ${clades[@]} 
echo ${clades[@]} >> ${6}/${1}.runtime.report
##retrieve subclade ids for each species to be analyzed
m=1
for i in ${clades[@]}
	do echo "Pathogen Number $m out of $2"
	name=$(grep ${6}/${1} -e [[:blank:]]${i}[[:blank:]] | cut -f 6 | head -n 1);
	export filename=$(echo "${6}/${name}.${i}.${3}clades" | sed -e 's/\s/_/g' -e 's/.gz//g' -e 's/output//g')
	[[ -a ${filename} ]] && rm "$filename"
	echo ${filename}
	echo "Finding the subclade ids for the species, ${name}, with clade number ${i}"; 
	awk -v OFS="\t" -v cnumber=$i '$2 == cnumber' /mnt/lustre/data/dseverson/poshTitan/titan/categories.dmp > "${filename}.subclades";
##retrieve read ids that mapped to all species subclades
	( gunzip -c $3 | perl /mnt/lustre/data/dseverson/poshTitan/titan/grab_readids.pl -q ${filename}.subclades > "${filename}" ) &
	(( m++ == $2 ))  && echo "Waiting for last species to finish before searching for reads!" &&  wait	
done
echo "The read ids for each species and all of its subclades have been retrieved!"

##Generate fastq file for each species, check whether two fastq files are required.
k=1
if [ -a "$5" ]; then
	echo "Analyzing paired fastq files on the fly!"
	cladefiles=$(ls ${6}/*.clades)
	echo ${#cladefiles[@]}
	(( ! $2 == ${#cladefiles[@]} )) && echo "There are not as many cladefiles as requested; something is wrong. Please address this!" && exit 2
	for j in ${cladefiles[@]}
	do 	echo "$j"
		cut -f 2 $j > ${j}.pattern.temp
		( gunzip -c "$4" | fgrep -w  -A3 -f ${j}.pattern.temp | grep -v "^--$" > ${j}_1.fastq ) & ##2>> ${1}.runtime.report ) & 
		( gunzip -c "$5" | fgrep -w  -A3 -f ${j}.pattern.temp | grep -v "^--$" > ${j}_2.fastq ) & ##2>> ${1}.runtime.report ) &
		( gunzip -c "$7" | fgrep -w -A3 -f ${j}.pattern.temp | grep -v "^--$" > ${j}_S.fastq ) & ##2 >> ${1}.runtime.report ) &
		echo $k
		(( k++ == $2 )) && echo "Waiting for all reads to be found" && wait;
	done
else
	echo "Analyzing fastq file on the fly!"
	cladefiles=$(ls *.clades)
        for j in ${cladefiles[@]}
	        do 	echo "$j"
		cut -f 2 $j > ${j}.pattern.temp
		( gunzip -c "$4" | grep -w -F -A3 -f .${j}.pattern.temp | grep -v "^--$" > ${j}.fastq ) & ##2>> ${1}.runtime.report ) &
		echo $k;
		(( k++ == $2 )) && wait && echo "Waiting for all reads to be found" && wait;
	done
fi

##Cleanup temp files
for j in ${cladefiles[@]}
	do rm ${j}.pattern.temp
done

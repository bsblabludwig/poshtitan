#!/bin/bash -l

#########################
# SGE SUBMISSION SCRIPT #
#########################

# Run in current working directory
#$ -cwd
# Use bash
#$ -S /bin/bash
# Export all variables present at job submission time
#$ -V
# Merge STDOUT and STDERR
#$ -j y
# Set the output file for STDOUT
#
#@ MONITOR $JOB_NAME.$JOB_ID.output
#$ -o $JOB_NAME.$JOB_ID.output
# Set the output file for STDERR
#
##@ MONITOR $JOB_NAME.$JOB_ID.error
##$ -e $JOB_NAME.$JOB_ID.error

# What is the name of your job?
#
#@ NAME pre-pTitan.map
#$ -N pre-pTitan.map

# Set the destination email address
#$ -M seversond12@mail.wlu.edu
# Set the conditions under which you wish to receive an email (b:begin, e:end, a:aborted, s:suspended)
#$ -m ea

#####################################
# SGE DIRECTIVES - Always set these
#####################################

# Expected RUNTIME
#
# Enter the expected runtime for your job. Specification of a shorter run time
# will cause the scheduler to be more likely to schedule your job sooner, but
# note that your job will be terminated if it exceeds the specified runtime.
#
# Format: hours:minutes:seconds, e.g.: 72:0:0
#$ -l h_rt=60:00:0

# Expected HARD MEMORY LIMIT (Per Slot)
#
# Enter the expected memory usage of your job.  Specification of a
# smaller memory requirement will cause the scheduler to be more
# likely to schedule your job sooner, but note that your job will be
# terminated if it exceeds the specified allocation.
#
# NB. This is the per slot memory limit, e.g. for a 2 slot job a 4G
# request would use 8G in total.
#
# Format: Memory required, defaults to MB, e.g. specify 4096 or 4G
# for 4 gigabytes.
#$ -l h_vmem=4.5G


# Specify parallel environment.
#
# Request parallel environment followed by the number of slots required.
# For example 'smp-verbose 2' will allocate 2 slots for the job. The
# more slots requsted the longer the job could take to launch and
# remember that you can't request more slots than your highest slot
# machine allows.
#$ -pe smp-verbose 6

###########################
# LOAD ENVIRONMENT MODULES
############################
# e.g.:
# module load apps/imb
# endsnippet:script:environment_module

###############################
# APPLICATION LAUNCH COMMANDS
###############################

#Load appropriate modules
source ~/.bashrc 

BASE=`cat ${INFO} | head -n $SGE_TASK_ID | tail -n 1`

##Map to the human genome and store only unmapped reads
echo "Mapping to the full hg19 using STAR splice-aware alignment"
STAR --runMode alignReads --runThreadN 6 --genomeDir /mnt/lustre/data/hg19/STARindex_hg19full_poshTitan_75o --readFilesIn /users/dseverson/sharedscratch/poshTitan_temp/${BASE}_filterdup_trimmed_1.fq /users/dseverson/sharedscratch/poshTitan_temp/${BASE}_filterdup_trimmed_2.fq  --outSAMattributes All --outReadsUnmapped Fastx --outSAMtype BAM Unsorted --chimSegmentMin 15 --chimJunctionOverhangMin 15 --alignMatesGapMax 200000 --alignIntronMax 200000 --outFileNamePrefix ${DIR}/${BASE}_

##Zip up quality trimmed reads and move to intermediate file store
if (( $? == 0 )); then
        rm /users/dseverson/sharedscratch/poshTitan_temp/${BASE}_filterdup_trimmed_[1,2,S].fq
else
        exit;
fi

echo "Removing unnecessary STAR files"
rm ${DIR}/${BASE}_Aligned.out.bam
rm ${DIR}/${BASE}_Log.out
rm ${DIR}/${BASE}_Log.progress.out
rm ${DIR}/${BASE}_Log.final.out
rm ${DIR}/${BASE}_Chimeric.out.sam
rm ${DIR}/${BASE}_Chimeric.out.junction 
rm ${DIR}/${BASE}_SJ.out.tab

echo "Moving and processing unmapped reads in preperation for the Kraken"
mv ${DIR}/${BASE}_Unmapped.out.mate1 ${DIR}/${BASE}_processed_1.fq 
mv ${DIR}/${BASE}_Unmapped.out.mate2 ${DIR}/${BASE}_processed_2.fq 

echo "Processing the line count files into read count files for easy R scripting"
wc -l ${DIR}/${BASE}_processed_1.fq >> ${DIR}/${BASE}.filterstats.txt
wc -l ${DIR}/${BASE}_processed_2.fq >> ${DIR}/${BASE}.filterstats.txt
wc -l ${DIR}/${BASE}_processed_S.fq >> ${DIR}/${BASE}.filterstats.txt
gzip ${DIR}/${BASE}_processed_2.fq &
gzip ${DIR}/${BASE}_processed_1.fq &
gzip ${DIR}/${BASE}_processed_S.fq &
wait;

perl -ne 'print if m/_[1,S]/' ${DIR}/${BASE}.filterstats.txt | awk -v FS='\t' -v BASE=$BASE  '{reads=$1/4; print reads "\t" BASE}' > ${DIR}/${BASE}.pre.txt
cut -d " " -f 2 ${DIR}/${BASE}.filterstats.txt | perl -ne 'print if m/_[1,S]/' | sed -e 's/.*filterdup_1.*/Duplicate Filtered Pairs/g' -e 's/.*filterdup_trimmed_1.*/Quality Filtered Pairs/g' -e 's/.*filterdup_trimmed_S.*/Quality Filtered Singlets/g' -e 's/.*_processed_1.fq/Human Filtered Pairs/g' -e 's/.*_1.fq/Total Read Pairs/g' -e 's/.*processed_S.fq/Human Filtered Singlets/g' > ${DIR}/${BASE}.columns.txt
paste ${DIR}/${BASE}.columns.txt ${DIR}/${BASE}.pre.txt > ${DIR}/${BASE}.filterreadstats.txt
rm ${DIR}/${BASE}.columns.txt
rm ${DIR}/${BASE}.pre.txt









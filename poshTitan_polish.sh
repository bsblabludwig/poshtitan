#!/bin/bash

##Written by David T. Severson
##Objective: to submit shell script variables to R for poshTitan final analysis
##usage $0 <Sample Name> <Directory of Samples>

R --slave --args $1 $2 < ~/job_scripts/poshTitan/poshTitan_sample_graph.R

#!/bin/bash -l

#########################
# SGE SUBMISSION SCRIPT #
#########################

# Run in current working directory
#$ -cwd
# Use bash
#$ -S /bin/bash
# Export all variables present at job submission time
#$ -V
# Merge STDOUT and STDERR
#$ -j y
# Set the output file for STDOUT
#
#@ MONITOR $JOB_NAME.$JOB_ID.output
#$ -o $JOB_NAME.$JOB_ID.output
# Set the output file for STDERR
#
##@ MONITOR $JOB_NAME.$JOB_ID.error
##$ -e $JOB_NAME.$JOB_ID.error

# What is the name of your job?
#
#@ NAME pre-pTitan.filter
#$ -N pre-pTitan.filter

# Set the destination email address
#$ -M seversond12@mail.wlu.edu
# Set the conditions under which you wish to receive an email (b:begin, e:end, a:aborted, s:suspended)
#$ -m ea

#####################################
# SGE DIRECTIVES - Always set these
#####################################

# Expected RUNTIME
#
# Enter the expected runtime for your job. Specification of a shorter run time
# will cause the scheduler to be more likely to schedule your job sooner, but
# note that your job will be terminated if it exceeds the specified runtime.
#
# Format: hours:minutes:seconds, e.g.: 72:0:0
#$ -l h_rt=20:00:0

# Expected HARD MEMORY LIMIT (Per Slot)
#
# Enter the expected memory usage of your job.  Specification of a
# smaller memory requirement will cause the scheduler to be more
# likely to schedule your job sooner, but note that your job will be
# terminated if it exceeds the specified allocation.
#
# NB. This is the per slot memory limit, e.g. for a 2 slot job a 4G
# request would use 8G in total.
#
# Format: Memory required, defaults to MB, e.g. specify 4096 or 4G
# for 4 gigabytes.
#$ -l h_vmem=30.5G


# Specify parallel environment.
#
# Request parallel environment followed by the number of slots required.
# For example 'smp-verbose 2' will allocate 2 slots for the job. The
# more slots requsted the longer the job could take to launch and
# remember that you can't request more slots than your highest slot
# machine allows.
#$ -pe smp-verbose 1

###########################
# LOAD ENVIRONMENT MODULES
############################
# e.g.:
# module load apps/imb
# endsnippet:script:environment_modules
module load apps/picard
module load apps/oraclejdk
source /users/dseverson/.bashrc


###############################
# APPLICATION LAUNCH COMMANDS
###############################

#Load appropriate modules
source ~/.bashrc 

BASE=`cut -f 2 ${INFO} | tail -n +2 | head -n $SGE_TASK_ID | tail -n 1 | cut -f 1`

##Remove low quality bases in reads and remove reads without sufficient information, then count reads with quality filtering complete
echo "Processing filtering sample ${BASE} low quality reads with sickle"
sickle pe -t sanger -q 20 -l 34 -f /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_1.fastq -r /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_2.fastq -o /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_1.fq -p /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_2.fq -s /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_S.fq
rm /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_[1,2].fastq 
wc -l /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_1.fq >> ${BASE}.filterstats.txt
wc -l /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_2.fq >> ${BASE}.filterstats.txt
wc -l /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_S.fq >> ${BASE}.filterstats.txt



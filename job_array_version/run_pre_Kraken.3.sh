#!/bin/bash -l

#########################
# SGE SUBMISSION SCRIPT #
#########################

# Run in current working directory
#$ -cwd
# Use bash
#$ -S /bin/bash
# Export all variables present at job submission time
#$ -V
# Merge STDOUT and STDERR
#$ -j y
# Set the output file for STDOUT
#
#@ MONITOR $JOB_NAME.$JOB_ID.output
#$ -o $JOB_NAME.$JOB_ID.output
# Set the output file for STDERR
#
##@ MONITOR $JOB_NAME.$JOB_ID.error
##$ -e $JOB_NAME.$JOB_ID.error

# What is the name of your job?
#
#@ NAME pre-pTitan.map
#$ -N pre-pTitan.map

# Set the destination email address
#$ -M david.severson@ludwig.ox.ac.uk
# Set the conditions under which you wish to receive an email (b:begin, e:end, a:aborted, s:suspended)
#$ -m beas

#####################################
# SGE DIRECTIVES - Always set these
#####################################

# Expected RUNTIME
#
# Enter the expected runtime for your job. Specification of a shorter run time
# will cause the scheduler to be more likely to schedule your job sooner, but
# note that your job will be terminated if it exceeds the specified runtime.
#
# Format: hours:minutes:seconds, e.g.: 72:0:0
#$ -l h_rt=60:00:0

# Expected HARD MEMORY LIMIT (Per Slot)
#
# Enter the expected memory usage of your job.  Specification of a
# smaller memory requirement will cause the scheduler to be more
# likely to schedule your job sooner, but note that your job will be
# terminated if it exceeds the specified allocation.
#
# NB. This is the per slot memory limit, e.g. for a 2 slot job a 4G
# request would use 8G in total.
#
# Format: Memory required, defaults to MB, e.g. specify 4096 or 4G
# for 4 gigabytes.
#$ -l h_vmem=4G


# Specify parallel environment.
#
# Request parallel environment followed by the number of slots required.
# For example 'smp-verbose 2' will allocate 2 slots for the job. The
# more slots requsted the longer the job could take to launch and
# remember that you can't request more slots than your highest slot
# machine allows.
#$ -pe smp-verbose 8

###########################
# LOAD ENVIRONMENT MODULES
############################
# e.g.:
# module load apps/imb
# endsnippet:script:environment_modules
module load apps/picard
module load apps/oraclejdk
source /users/dseverson/.bashrc


###############################
# APPLICATION LAUNCH COMMANDS
###############################

#Load appropriate modules
source ~/.bashrc 

BASE=`head -n $SGE_TASK_ID job_array_list.txt | tail -n 1 | cut -f 1`

##Map to the human genome and store only unmapped reads
echo "Mapping to the full hg19 using bowtie2 end to end alignment"
bowtie2 -X 800 -N 1 --very-sensitive -p 8 -x /mnt/lustre/data/hg19/bowtie2/hg19_full -1 /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_1.fq -2 /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_2.fq -U /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_S.fq | samtools view -bS -F 0x02 - > /users/dseverson/sharedscratch/SY_temp/${BASE}.unmapped.bam

##Zip up quality trimmed reads and move to intermediate file store
rm /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_[1,2,S].fq




#!/bin/bash

##Written by David T. Severson
##Objective: to submit shell script variables to R for poshTitan final analysis
##usage $0 

cd /users/dseverson/sharedscratch/SY_poshTitan_results/SY_Gastric_samples/
##wholesale expression
for i in $(echo *_analysis/binary*whole.sample*) 
	do echo $i 
	head -n 1 $i > header.txt
	tail -n +2 $i >>  pre.cohort.summary.txt  
done 
cat header.txt pre.cohort.summary.txt > cohort.summary.txt
rm pre.cohort.summary.txt
rm header.txt

##Single Gene 
for i in $(echo *_analysis/binary*single_gene*)
        do echo $i 
        head -n 1 $i > header.txt 
        tail -n +2 $i >>  pre.cohort.summary.txt
done
cat header.txt pre.cohort.summary.txt > cohort.summary.single_gene.txt
rm pre.cohort.summary.txt
rm header.txt


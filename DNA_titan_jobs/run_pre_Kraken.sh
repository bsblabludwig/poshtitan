#!/bin/bash -l

#########################
# SGE SUBMISSION SCRIPT #
#########################

# Run in current working directory
#$ -cwd
# Use bash
#$ -S /bin/bash
# Export all variables present at job submission time
#$ -V
# Merge STDOUT and STDERR
#$ -j y
# Set the output file for STDOUT
#
#@ MONITOR $JOB_NAME.$JOB_ID.output
#$ -o $JOB_NAME.$JOB_ID.output
# Set the output file for STDERR
#
##@ MONITOR $JOB_NAME.$JOB_ID.error
##$ -e $JOB_NAME.$JOB_ID.error

# What is the name of your job?
#
#@ NAME pre-pTitan
#$ -N pre-pTitan

# Set the destination email address
#$ -M david.severson@ludwig.ox.ac.uk
# Set the conditions under which you wish to receive an email (b:begin, e:end, a:aborted, s:suspended)
#$ -m beas

#####################################
# SGE DIRECTIVES - Always set these
#####################################

# Expected RUNTIME
#
# Enter the expected runtime for your job. Specification of a shorter run time
# will cause the scheduler to be more likely to schedule your job sooner, but
# note that your job will be terminated if it exceeds the specified runtime.
#
# Format: hours:minutes:seconds, e.g.: 72:0:0
#$ -l h_rt=164:00:0

# Expected HARD MEMORY LIMIT (Per Slot)
#
# Enter the expected memory usage of your job.  Specification of a
# smaller memory requirement will cause the scheduler to be more
# likely to schedule your job sooner, but note that your job will be
# terminated if it exceeds the specified allocation.
#
# NB. This is the per slot memory limit, e.g. for a 2 slot job a 4G
# request would use 8G in total.
#
# Format: Memory required, defaults to MB, e.g. specify 4096 or 4G
# for 4 gigabytes.
#$ -l h_vmem=15.5G


# Specify parallel environment.
#
# Request parallel environment followed by the number of slots required.
# For example 'smp-verbose 2' will allocate 2 slots for the job. The
# more slots requsted the longer the job could take to launch and
# remember that you can't request more slots than your highest slot
# machine allows.
#$ -pe smp-verbose 8

###########################
# LOAD ENVIRONMENT MODULES
############################
# e.g.:
# module load apps/imb
# endsnippet:script:environment_modules
module load apps/picard
module load apps/oraclejdk
source /users/dseverson/.bashrc


###############################
# APPLICATION LAUNCH COMMANDS
###############################

#Load appropriate modules
source ~/.bashrc 

##Count raw files and unzip them
echo "Processing filtering fastq files for duplicates" 
gunzip -c ${BASE}_1.fq.gz > /users/dseverson/sharedscratch/SY_temp/${BASE}_1.fq
gunzip -c ${BASE}_2.fq.gz > /users/dseverson/sharedscratch/SY_temp/${BASE}_2.fq
wc -l /users/dseverson/sharedscratch/SY_temp/${BASE}_1.fq > ${BASE}.filterstats.txt
wc -l /users/dseverson/sharedscratch/SY_temp/${BASE}_2.fq >> ${BASE}.filterstats.txt

##Filter raw files for duplicates and count files without duplicates and rezip raw files
filter_duplicates_parallel.pl -o /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup /users/dseverson/sharedscratch/SY_temp/${BASE}_1.fq /users/dseverson/sharedscratch/SY_temp/${BASE}_2.fq
rm /users/dseverson/sharedscratch/SY_temp/${BASE}_1.fq
rm /users/dseverson/sharedscratch/SY_temp/${BASE}_2.fq
wc -l /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_1.fastq >> ${BASE}.filterstats.txt
wc -l /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_2.fastq >> ${BASE}.filterstats.txt


##Remove low quality bases in reads and remove reads without sufficient information, then count reads with quality filtering complete
echo "Processing filtering low quality reads"
sickle pe -t sanger -q 20 -l 34 -f /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_1.fastq -r /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_2.fastq -o /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_1.fq -p /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_2.fq -s /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_S.fq
rm /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_[1,2].fastq 
wc -l /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_1.fq >> ${BASE}.filterstats.txt
wc -l /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_2.fq >> ${BASE}.filterstats.txt
wc -l /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_S.fq >> ${BASE}.filterstats.txt

##Map to the human genome and store only unmapped reads
echo "Mapping to the full hg19 using bowtie2 end to end alignment"
bowtie2 -X 800 -N 1 --very-sensitive -p 8 -x /mnt/lustre/data/hg19/bowtie2/hg19_full -1 /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_1.fq -2 /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_2.fq -U /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_S.fq | samtools view -bS -F 0x02 - > /users/dseverson/sharedscratch/SY_temp/${BASE}.unmapped.bam

##Zip up quality trimmed reads and move to intermediate file store
rm /users/dseverson/sharedscratch/SY_temp/${BASE}_filterdup_trimmed_[1,2,S].fq


##Creating final processed reads filtered for duplicates, low information, and human reads
echo "Dumping new fastq files of unmapped reads which represent fully pre-processed reads to run the Kraken on and count number of lines in these files"
SamToFastq INPUT=/users/dseverson/sharedscratch/SY_temp/${BASE}.unmapped.bam FASTQ=${BASE}_processed_1.fq SECOND_END_FASTQ=${BASE}_processed_2.fq UNPAIRED_FASTQ=${BASE}_processed_S.fq VALIDATION_STRINGENCY=SILENT
wc -l ${BASE}_processed_1.fq >> ${BASE}.filterstats.txt
wc -l ${BASE}_processed_2.fq >> ${BASE}.filterstats.txt
wc -l ${BASE}_processed_S.fq >> ${BASE}.filterstats.txt
gzip ${BASE}_processed_2.fq
gzip ${BASE}_processed_1.fq
gzip ${BASE}_processed_S.fq
rm /users/dseverson/sharedscratch/SY_temp/${BASE}.unmapped.bam

echo "Processing the line count files into read count files for easy R scripting"
perl -ne 'print if m/_[1,S]/' ${BASE}.filterstats.txt | awk -v FS='\t' -v BASE=$BASE  '{reads=$1/4; print reads "\t" BASE}' > ${BASE}.pre.txt
cut -d " " -f 2 ${BASE}.filterstats.txt | perl -ne 'print if m/_[1,S]/' | sed -e 's/.*filterdup_1.*/Duplicate Filtered Pairs/g' -e 's/.*filterdup_trimmed_1.*/Quality Filtered Pairs/g' -e 's/.*filterdup_trimmed_S.*/Quality Filtered Singlets/g' -e 's/.*_processed_1.fq/Human Filtered Pairs/g' -e 's/.*_1.fq/Total Read Pairs/g' -e 's/.*processed_S.fq/Human Filtered Singlets/g' > ${BASE}.columns.txt
paste ${BASE}.columns.txt ${BASE}.pre.txt > ${BASE}.filterreadstats.txt
rm ${BASE}.columns.txt
rm ${BASE}.pre.txt


#!/bin/bash -l

#########################
# SGE SUBMISSION SCRIPT #
#########################

# Run in current working directory
#$ -cwd
# Use bash
#$ -S /bin/bash
# Export all variables present at job submission time
#$ -V
# Merge STDOUT and STDERR
#$ -j y
# Set the output file for STDOUT
#
#@ MONITOR $JOB_NAME.$JOB_ID.output
#$ -o $JOB_NAME.$JOB_ID.output
# Set the output file for STDERR
#
##@ MONITOR $JOB_NAME.$JOB_ID.error
##$ -e $JOB_NAME.$JOB_ID.error

# What is the name of your job?
#
#@ NAME pre-pTitan.final
#$ -N pre-pTitan.final

# Set the destination email address
#$ -M david.severson@ludwig.ox.ac.uk
# Set the conditions under which you wish to receive an email (b:begin, e:end, a:aborted, s:suspended)
#$ -m beas

#####################################
# SGE DIRECTIVES - Always set these
#####################################

# Expected RUNTIME
#
# Enter the expected runtime for your job. Specification of a shorter run time
# will cause the scheduler to be more likely to schedule your job sooner, but
# note that your job will be terminated if it exceeds the specified runtime.
#
# Format: hours:minutes:seconds, e.g.: 72:0:0
#$ -l h_rt=10:00:0

# Expected HARD MEMORY LIMIT (Per Slot)
#
# Enter the expected memory usage of your job.  Specification of a
# smaller memory requirement will cause the scheduler to be more
# likely to schedule your job sooner, but note that your job will be
# terminated if it exceeds the specified allocation.
#
# NB. This is the per slot memory limit, e.g. for a 2 slot job a 4G
# request would use 8G in total.
#
# Format: Memory required, defaults to MB, e.g. specify 4096 or 4G
# for 4 gigabytes.
#$ -l h_vmem=8G


# Specify parallel environment.
#
# Request parallel environment followed by the number of slots required.
# For example 'smp-verbose 2' will allocate 2 slots for the job. The
# more slots requsted the longer the job could take to launch and
# remember that you can't request more slots than your highest slot
# machine allows.
#$ -pe smp-verbose 3

###########################
# LOAD ENVIRONMENT MODULES
############################
# e.g.:
# module load apps/imb
# endsnippet:script:environment_modules
module load apps/picard
module load apps/oraclejdk
source /users/dseverson/.bashrc


###############################
# APPLICATION LAUNCH COMMANDS
###############################

#Load appropriate modules
source ~/.bashrc 

BASE=`head -n $SGE_TASK_ID job_array_list.txt | tail -n 1 | cut -f 1`

##Creating final processed reads filtered for duplicates, low information, and human reads

echo "Dumping new ${BASE} fastq files of unmapped reads which represent fully pre-processed reads to run the Kraken on and count number of lines in these files"
SamToFastq INPUT=/users/dseverson/sharedscratch/SY_temp/${BASE}.unmapped.bam FASTQ=${BASE}_processed_1.fq SECOND_END_FASTQ=${BASE}_processed_2.fq UNPAIRED_FASTQ=${BASE}_processed_S.fq VALIDATION_STRINGENCY=SILENT
wc -l ${BASE}_processed_1.fq >> ${BASE}.filterstats.txt
wc -l ${BASE}_processed_2.fq >> ${BASE}.filterstats.txt
wc -l ${BASE}_processed_S.fq >> ${BASE}.filterstats.txt
gzip ${BASE}_processed_2.fq &
gzip ${BASE}_processed_1.fq &
gzip ${BASE}_processed_S.fq &
wait;
rm /users/dseverson/sharedscratch/SY_temp/${BASE}.unmapped.bam

echo "Processing the line count files into read count files for easy R scripting"
perl -ne 'print if m/_[1,S]/' ${BASE}.filterstats.txt | awk -v FS='\t' -v BASE=$BASE  '{reads=$1/4; print reads "\t" BASE}' > ${BASE}.pre.txt
cut -d " " -f 2 ${BASE}.filterstats.txt | perl -ne 'print if m/_[1,S]/' | sed -e 's/.*filterdup_1.*/Duplicate Filtered Pairs/g' -e 's/.*filterdup_trimmed_1.*/Quality Filtered Pairs/g' -e 's/.*filterdup_trimmed_S.*/Quality Filtered Singlets/g' -e 's/.*_processed_1.fq/Human Filtered Pairs/g' -e 's/.*_1.fq/Total Read Pairs/g' -e 's/.*processed_S.fq/Human Filtered Singlets/g' > ${BASE}.columns.txt
paste ${BASE}.columns.txt ${BASE}.pre.txt > ${BASE}.filterreadstats.txt
rm ${BASE}.columns.txt
rm ${BASE}.pre.txt


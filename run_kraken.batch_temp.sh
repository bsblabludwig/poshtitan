#!/bin/bash -l

#########################
# SGE SUBMISSION SCRIPT #
#########################

# Run in current working directory
#$ -cwd
# Use bash
#$ -S /bin/bash
# Export all variables present at job submission time
#$ -V
# Merge STDOUT and STDERR
#$ -j y
# Set the output file for STDOUT
#
#@ MONITOR $JOB_NAME.$JOB_ID.output
#$ -o $JOB_NAME.$JOB_ID.output
# Set the output file for STDERR
#
##@ MONITOR $JOB_NAME.$JOB_ID.error
##$ -e $JOB_NAME.$JOB_ID.error

# What is the name of your job?
#
#@ NAME kraken
#$ -N kraken

# Set the destination email address
#$ -M david.severson@ludwig.ox.ac.uk
# Set the conditions under which you wish to receive an email (b:begin, e:end, a:aborted, s:suspended)
#$ -m beas

#####################################
# SGE DIRECTIVES - Always set these
#####################################

# Expected RUNTIME
#
# Enter the expected runtime for your job. Specification of a shorter run time
# will cause the scheduler to be more likely to schedule your job sooner, but
# note that your job will be terminated if it exceeds the specified runtime.
#
# Format: hours:minutes:seconds, e.g.: 72:0:0
#$ -l h_rt=10:00:0

##Specify node use
#$ -q *@node01.prv.cortex.cluster
#$ -l exclusive=true

# Expected HARD MEMORY LIMIT (Per Slot)
#
# Enter the expected memory usage of your job.  Specification of a
# smaller memory requirement will cause the scheduler to be more
# likely to schedule your job sooner, but note that your job will be
# terminated if it exceeds the specified allocation.
#
# NB. This is the per slot memory limit, e.g. for a 2 slot job a 4G
# request would use 8G in total.
#
# Format: Memory required, defaults to MB, e.g. specify 4096 or 4G
# for 4 gigabytes.
#$ -l h_vmem=15G

# Specify parallel environment.
#
# Request parallel environment followed by the number of slots required.
# For example 'smp-verbose 2' will allocate 2 slots for the job. The
# more slots requsted the longer the job could take to launch and
# remember that you can't request more slots than your highest slot
# machine allows.
#$ -pe smp-verbose 16


###########################
# LOAD ENVIRONMENT MODULES
############################
# e.g.:
# module load apps/imb
# endsnippet:script:environment_modules
source /users/dseverson/.bashrc

###############################
# APPLICATION LAUNCH COMMANDS
###############################

echo "Copying kraken database into node01 RAMdisk; exclusive use of the node should be set"
#cp -a /mnt/lustre/data/kraken/kraken_db /dev/shm
echo "Combining single end and paired end results";
for i in $(echo *processed_1.fq.gz); 
	do BASE=`echo $i | sed -e 's/_1.fq.gz//g'`
	cat ${BASE}_P.kraken.output ${BASE}_S.kraken,output | gzip -c > ${BASE}.kraken.output.gz
	rm ${BASE}_P.kraken.output
	rm ${BASE}_S.kraken.output
done

echo "removing kraken database from RAMdisk"
rm -r /dev/shm/kraken_db
ls /dev/shm

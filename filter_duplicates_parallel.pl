#!/usr/bin/env perl
###########################################################
#
# Filter a fastq file to retail only unique sequences
#
###########################################################
my $tmpdir;
BEGIN {
    $tmpdir = "/tmp/".($ENV{USER}||$ENV{USERNAME}||$$);
    mkdir $tmpdir;
};

use Inline (CPP => 'DATA',
            DIRECTORY => "$tmpdir");
use strict;
use warnings;
use IO::File;
use Log::Log4perl qw(:easy);
use Getopt::Std;
use vars qw(%args $isPairedEnd);
getopts('qs:o:iv', \%args);

if ($args{i}){
    Log::Log4perl->easy_init( { level    => $INFO,
                            layout   => '%d %p> %M'."\t".'%m%n' 
                           },
                        );
} elsif ($args{v}) {
    Log::Log4perl->easy_init( { level    => $DEBUG,
                            layout   => '%d %p> %M'."\t".'%m%n' 
                           },
                        );
} else {
    Log::Log4perl->easy_init( { level    => $WARN,
                            layout   => '%d %p> %M'."\t".'%m%n' 
                           },
                        );
}

my $seed = $args{s} || int(rand(2**32-1));

my @files = @ARGV;
die "Usage: $0 [-o output_prefix] seq1.fq [seq2.fq]\n" unless @files && @files <= 2;
$isPairedEnd = @files > 1;
my @outHandles;
if ($args{o}) {
    if ($isPairedEnd) {
        $outHandles[0] = IO::File->new($args{o}."_1.fastq", '>:raw:perlio') or die "Error writing to ".$args{o}."_1.fastq: $!\n";
        $outHandles[1] = IO::File->new($args{o}."_2.fastq", '>:raw:perlio') or die "Error writing to ".$args{o}."_2.fastq: $!\n";
        #WARN dirty hack: use the first part of the sequence string as a separator
    } else {
        $outHandles[0] = IO::File->new($args{o}.".fastq", '>:raw:perlio') or die "Error writing to ".$args{o}.".fastq: $!\n";
    }
} else {
    WARN("Writing paired-end output to STDOUT will produce interleaved format!") if ($isPairedEnd);
    $outHandles[0] = \*STDOUT;
}

my @inHandles = map {IO::File->new($_, '<:raw:perlio') or die "Error reading $_: $!\n"} @files;

INFO("First pass: reading input files...");
my $seqs = read_input(@inHandles);
INFO("Second pass: sorting data...");
my $pos = sort_keys($seqs);
undef $seqs; # free
INFO("Third pass: writing files...");
write_output($pos, \@inHandles, \@outHandles);

close($_) for @inHandles;
close($_) for @outHandles;

#=========================== Methods ===========================#
sub sort_keys {
    my $seqs = shift;
    return [sort {$a->[0] <=> $b->[0]} values %$seqs];
    
#    my $data = [];
#    my @keys = keys %$seqs;
#    foreach my $key (@keys) {
#        DEBUG("Sorted ".scalar(@$data)." reads out of ".scalar(@keys)) if $args{v} && !(scalar(@$data) % 1000);
#        $data = sorted_insert($seqs->{$key}, $data);
#        # free incrementally
#        delete($seqs->{$key});
#    }
#    return $data;
}

sub read_input {
    my @inHandles = @_;
    my $c=0;
    my %seqs;
    while (my $seq1 = read_next_chunk($inHandles[0])) {
        DEBUG("Read $c reads") unless ($args{q}) || ++$c % 10000;
        
        if ($isPairedEnd) {
            my $seq2 = read_next_chunk($inHandles[1]);
            die "ERROR: Fewer reads in ".$files[1]." than in ".$files[0]."\n" if !$args{q} && !defined $seq2;
            my ($d1, $d2) = ($seq1->[1], $seq2->[1]);
            unless ($args{q}) {
                my $sLen=length($d1->[0])-1;
                die "ERROR: Read pairs not matching: ".$d1->[0]." <-> ".$d2->[0]."\n" unless unpack("A$sLen", $d1->[0]) eq unpack("A$sLen", $d2->[0]);
            }
        
            my $crc = mur_hash($d1->[1]."\t".$d2->[1], $seed);
            my $score = calculate_score($d1->[2].$d2->[2]);
            #DEBUG(sub { "$d1->[1]\t$d2->[1] => $crc" });
            if (exists $seqs{$crc}){
                $seqs{$crc} = [$seq1->[0], $seq2->[0], $score] if ($seqs{$crc}->[2] < $score);
            } else {
                $seqs{$crc} = [$seq1->[0], $seq2->[0], $score];
            }
        } else {
            my $d1 = $seq1->[1];
            my $crc = mur_hash($d1->[1], $seed);
            my $score = calculate_score($d1->[2]);
        
            if (exists $seqs{$crc}){
                $seqs{$crc} = [$seq1->[0], undef, $score] if ($seqs{$crc}->[2] < $score);
            } else {
                $seqs{$crc} = [$seq1->[0], undef, $score];
            }
        }
    }
    return \%seqs;
}

sub write_output {
    my ($positions, $inHandles, $outHandles) = @_;
    
    my $lastPos1 = 0;
    my $lastPos2 = 0;
    $inHandles->[0]->seek($lastPos1, 0);
    $inHandles->[1]->seek($lastPos2, 0) if $isPairedEnd;
    my $hc = scalar(@$outHandles);
    my $c=0;
    foreach my $pair (@$positions) {
        DEBUG("Wrote $c reads") if $args{v} && !++$c % 10000;
        
        if ($isPairedEnd) {
            my ($p1, $p2) = @$pair;
            $inHandles->[0]->seek($p1, 0);
            my $chunk1 = read_next_chunk($inHandles->[0], 1)->[1];
            $inHandles->[1]->seek($p2, 0);
            my $chunk2 = read_next_chunk($inHandles->[1], 1)->[1];
#            $lastPos1=$p1;
#            $lastPos2=$p2;
            
            if ($hc > 1) {
                $outHandles->[0]->printf("%s\n%s\n+\n%s\n", @$chunk1);
                $outHandles->[1]->printf("%s\n%s\n+\n%s\n", @$chunk2);
            } else {
                $outHandles->[0]->printf("%s\n%s\n+\n%s\n", @$chunk1);
                $outHandles->[0]->printf("%s\n%s\n+\n%s\n", @$chunk2);
            }
        } else {
            my ($p1) = @$pair;
            $inHandles->[0]->seek($p1, 0);
            my $chunk1 = read_next_chunk($inHandles->[0], 1)->[1];
#            $lastPos1=$p1;
            $outHandles->[0]->printf("%s\n%s\n+\n%s\n", @$chunk1);
        }
    }
}

sub read_next_chunk {
    my $fh = shift;
    my $dontTell = shift;
    my $pos;
    $pos = $fh->tell unless $dontTell;
    my $data = []; # save seek position in file
    for (1..4) {
        my $l = <$fh>;
        return undef unless $l;
        next if $_==3; # skip the +
        
        chomp($l);
        push @$data, $l;
    }
    return [$pos, $data];
}

__END__
__CPP__
#include <stdlib.h>
#include <stdint.h>

#define	FORCE_INLINE inline __attribute__((always_inline))

inline uint32_t rotl32 ( uint32_t x, int8_t r )
{
  return (x << r) | (x >> (32 - r));
}

inline uint64_t rotl64 ( uint64_t x, int8_t r )
{
  return (x << r) | (x >> (64 - r));
}

#define	ROTL32(x,y)	rotl32(x,y)
#define ROTL64(x,y)	rotl64(x,y)

#define BIG_CONSTANT(x) (x##LLU)

//-----------------------------------------------------------------------------
// Block read - if your platform needs to do endian-swapping or can only
// handle aligned reads, do the conversion here

FORCE_INLINE uint32_t getblock32 ( const uint32_t * p, int i )
{
  return p[i];
}

FORCE_INLINE uint64_t getblock64 ( const uint64_t * p, int i )
{
  return p[i];
}

//-----------------------------------------------------------------------------
// Finalization mix - force all bits of a hash block to avalanche

FORCE_INLINE uint32_t fmix32 ( uint32_t h )
{
  h ^= h >> 16;
  h *= 0x85ebca6b;
  h ^= h >> 13;
  h *= 0xc2b2ae35;
  h ^= h >> 16;

  return h;
}

//----------

FORCE_INLINE uint64_t fmix64 ( uint64_t k )
{
  k ^= k >> 33;
  k *= BIG_CONSTANT(0xff51afd7ed558ccd);
  k ^= k >> 33;
  k *= BIG_CONSTANT(0xc4ceb9fe1a85ec53);
  k ^= k >> 33;

  return k;
}

//-----------------------------------------------------------------------------

void MurmurHash3_x64_128 ( const void * key, const int len,
                           const uint32_t seed, void * out )
{
  const uint8_t * data = (const uint8_t*)key;
  const int nblocks = len / 16;

  uint64_t h1 = seed;
  uint64_t h2 = seed;

  const uint64_t c1 = BIG_CONSTANT(0x87c37b91114253d5);
  const uint64_t c2 = BIG_CONSTANT(0x4cf5ad432745937f);

  //----------
  // body

  const uint64_t * blocks = (const uint64_t *)(data);

  for(int i = 0; i < nblocks; i++)
  {
    uint64_t k1 = getblock64(blocks,i*2+0);
    uint64_t k2 = getblock64(blocks,i*2+1);

    k1 *= c1; k1  = ROTL64(k1,31); k1 *= c2; h1 ^= k1;

    h1 = ROTL64(h1,27); h1 += h2; h1 = h1*5+0x52dce729;

    k2 *= c2; k2  = ROTL64(k2,33); k2 *= c1; h2 ^= k2;

    h2 = ROTL64(h2,31); h2 += h1; h2 = h2*5+0x38495ab5;
  }

  //----------
  // tail

  const uint8_t * tail = (const uint8_t*)(data + nblocks*16);

  uint64_t k1 = 0;
  uint64_t k2 = 0;

  switch(len & 15)
  {
  case 15: k2 ^= ((uint64_t)tail[14]) << 48;
  case 14: k2 ^= ((uint64_t)tail[13]) << 40;
  case 13: k2 ^= ((uint64_t)tail[12]) << 32;
  case 12: k2 ^= ((uint64_t)tail[11]) << 24;
  case 11: k2 ^= ((uint64_t)tail[10]) << 16;
  case 10: k2 ^= ((uint64_t)tail[ 9]) << 8;
  case  9: k2 ^= ((uint64_t)tail[ 8]) << 0;
           k2 *= c2; k2  = ROTL64(k2,33); k2 *= c1; h2 ^= k2;

  case  8: k1 ^= ((uint64_t)tail[ 7]) << 56;
  case  7: k1 ^= ((uint64_t)tail[ 6]) << 48;
  case  6: k1 ^= ((uint64_t)tail[ 5]) << 40;
  case  5: k1 ^= ((uint64_t)tail[ 4]) << 32;
  case  4: k1 ^= ((uint64_t)tail[ 3]) << 24;
  case  3: k1 ^= ((uint64_t)tail[ 2]) << 16;
  case  2: k1 ^= ((uint64_t)tail[ 1]) << 8;
  case  1: k1 ^= ((uint64_t)tail[ 0]) << 0;
           k1 *= c1; k1  = ROTL64(k1,31); k1 *= c2; h1 ^= k1;
  };

  //----------
  // finalization

  h1 ^= len; h2 ^= len;

  h1 += h2;
  h2 += h1;

  h1 = fmix64(h1);
  h2 = fmix64(h2);

  h1 += h2;
  h2 += h1;

  ((uint64_t*)out)[0] = h1;
  ((uint64_t*)out)[1] = h2;
}

//-----------------------------------------------------------------------------

SV * mur_hash (const char *sequence, const int seed) {
    uint64_t out[2];
    MurmurHash3_x64_128(sequence, strlen(sequence), seed, &out);

    // convert to hex
    char *o;
    o = new char[32 + 1];
    sprintf(o, "%016llX%016llX", out[0], out[1]);
//    fprintf(stderr, "%s %d\n", o, strlen(o));
    SV * ret;
    ret = newSVpv(o, 0);
    free(o);
    return ret;
}

int calculate_score (const char *scoreString) {
    int result = 0;
    while (*scoreString){
        result += *scoreString - 33;
        scoreString++;
    }
    return result;
}